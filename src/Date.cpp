# include "../include/Date.hpp"
# include <iostream>



Date::Date(int year, int month, int day)
: _year(year),
  _month(month),
  _day(day),
  _weekDay(DayOfTheWeek_UNKNOWN)
{   
}

Date::~Date() = default;

Date::Date(const Date &p)
: _year(p._year),
  _month(p._month),
  _day(p._day),
  _weekDay(p._weekDay),
  _holidays(p._holidays)
{
}

Date &Date::operator=(const Date &p)
{
    if (&p != this)
    {
        _day = p._day;
        _month = p._month;
        _year = p._year;
        _weekDay = p._weekDay;
        _holidays = p._holidays;
    }
    return *this;
}

bool Date::operator<(const Date &d) const
{
    if (_year < d._year) {
        return true;
    }
    else if (_year == d._year && _month < d._month) {
        return true;
    }
    else if (_year == d._year && _month == d._month && _day < d._day) {
        return true;
    }

    return false;
}

bool Date::operator==(const Date &d)
{
    return (d._day == _day) && (d._month == _month) && (d._year == _year);
}

void Date::setHolidays(const std::set<Date> &days)
{
    _holidays = days;
}

bool Date::isHoliday()
{
    return _holidays.find(*this) != _holidays.end();
}

DayOfTheWeek Date::dayOfTheWeek()
{
    if (_weekDay != DayOfTheWeek_UNKNOWN)
    {
        return _weekDay;
    }
    int day = 1;
    static Date d(1900, 1, 1);
    for (;d < *this; ++d)
    {
        if (day == 6)
        {
            day = 0;
        }
        else
        {
            day++;
        }
    }

    _weekDay = static_cast<DayOfTheWeek>(day);
    return _weekDay;
}

std::string Date::month()
{
    switch (_month) 
    {
        case Month_January: return "January";
        case Month_February: return "February";
        case Month_March: return "March";
        case Month_April: return "April";
        case Month_May: return "May";
        case Month_June: return "June";
        case Month_July: return "July";
        case Month_August: return "August";
        case Month_September: return "September";
        case Month_October: return "October";
        case Month_November: return "November";
        case Month_December: return "December";
        default: throw std::runtime_error("unknown month");
    }
    return "";
}

std::string Date::dayOfWeek()
{
    switch (this->dayOfTheWeek()) 
    {
        case DayOfTheWeek_Sunday: return "Sunday";
        case DayOfTheWeek_Monday: return "Monday";
        case DayOfTheWeek_Tuesday: return "Tuesday";
        case DayOfTheWeek_Wednesday: return "Wednesday";
        case DayOfTheWeek_Thursday: return "Thursday";
        case DayOfTheWeek_Friday: return "Friday";
        case DayOfTheWeek_Saturday: return "Saturday";
        default: throw std::runtime_error("unknown day of week");
    }
}

void Date::add(int numDays)
{
    for (int i = 0; i < numDays; ++i)
    {
        ++*this;
    }
}

void Date::addTradingDays(int numDays)
{
    while (!isTradingDay())
    {
        ++*this;
    }
    for (int i = 0; i < numDays; ++i)
    {
        ++*this;
    }
}

void Date::subtract(int numDays)
{
    for (int i = 0; i < numDays; ++i)
    {
        --*this;
    }
}

void Date::subtractTradingDays(int numDays)
{
    while (!isTradingDay())
    {
        --*this;
    }
    for (int i = 0; i < numDays; ++i)
    {
        --*this;
        while (!isTradingDay())
        {
            --*this;
        }
    }
}

int Date::dateDifference(const Date &date)
{
    Date d = *this;
    if (d < date)
    {
        int diff = 0;
        while(d < date) 
        {
            ++d;
            ++diff;
        }
        return diff;
    }

    int diff = 0;
    while (date < d)
    {
        --d;
        --diff;
    }
    return diff;
}

int Date::tradingDateDifference(const Date &date)
{
    Date d = *this;
    if (d < date)
    {
        int diff = 0;
        while (!d.isTradingDay())
        {
            ++d;
        }
        while (d < date)
        {
            ++d;
            ++diff;
            while (!d.isTradingDay())
            {
                ++d;
            }
        }
        return diff;
    }

    int diff = 0;
    while (!d.isTradingDay())
    {
        --d;
    }
    while (date < d)
    {
        --d;
        --diff;
        while (!d.isTradingDay())
        {
            --d;
        }
    }
    return diff;
}

bool Date::isWeekDay()
{
    DayOfTheWeek dayOfWeek  = dayOfTheWeek();
    if (dayOfWeek == DayOfTheWeek_Sunday || dayOfWeek == DayOfTheWeek_Saturday)
    {
        return false;
    }
    return true;
}

bool Date::isTradingDay()
{
    if (!isWeekDay()) {
        return false;
    }
    else if (_holidays.size() == 0) {
        return true;
    }
    else if (isHoliday()) {
        return false;
    }
    return true;
}

Date Date::nextTradingDay()
{
    Date d = *this;
    if (d.isTradingDay())
    {
        return ++d;
    }
    while (!d.isTradingDay()) {
        ++d;
    }
    return d;
}

bool Date::isLeapYear()
{
    if (_year%4 != 0) {
        return false;
    }
    if (_year%100 != 0) {
        return false;
    }
    if (_year %400 != 0) {
        return false;
    }
    return true;
}

Date &Date::operator--()
{
    if (_weekDay != DayOfTheWeek_UNKNOWN)
    {
        if (_weekDay == DayOfTheWeek_Sunday) {
            _weekDay = DayOfTheWeek_Saturday;
        }
        else
        {
            _weekDay = static_cast<DayOfTheWeek>(_weekDay -  1);
        }
    }
    else if (_day > 1)
    {
        _day--;
        return *this;
    }
    else if (_month == Month_January)
    {
        _month = Month_December;
        _day = 31;
        _year--;
        return *this;
    }
    _month--;
    if (_month == Month_February) {
        _day = isLeapYear() ? 29 : 28;
        return *this;
    }

    std::set<int> monthsWithThirtyOneDays = {1, 3, 5, 7, 8, 10, 12};
    if (monthsWithThirtyOneDays.find(_month) != monthsWithThirtyOneDays.end()) {
        _day = 31;
    }
    else
    {
        _day = 30;
    }
    return *this;
}

Date &Date::operator++()
{
    std::set<int> monthsWithThirtyOneDays = {1, 3, 5, 7, 8, 10, 12};
    if (_day == 31)
    {
        _day = 1;
        _month++;
    }
    else if (_day == 30 and monthsWithThirtyOneDays.find(_month) != monthsWithThirtyOneDays.end()) {
        _day = 1;
        _month++;
    }
    else if (_day == 29 and _month == 2)
    {
        _day = 1;
        _month++;
    }
    else if (_day == 28 and _month == 2 and !isLeapYear())
    {
        _day = 1;
        _month++;
    }
    else 
    {
        _day++;
    }
    if (_month > 12) {
        _month =  1;
        _year++;
    }
    if (_weekDay != DayOfTheWeek_UNKNOWN)
    {
        if (_weekDay == DayOfTheWeek_Saturday) {
            _weekDay = DayOfTheWeek_Sunday;
        }
        else {
            _weekDay = static_cast<DayOfTheWeek>(_weekDay + 1);
        }
    }
    return *this;
}

void Date::print()
{
    std::cout << _year << "/" << _month << "/" << _day << "\n";
}
