# include "../include/GenericOption.hpp"

GenericOption::GenericOption(PriceType strike, OptionType type, PriceType cost)
: _strike(strike),
  _type(type),
  _cost(cost)
{

}

GenericOption::GenericOption(const GenericOption &p) 
: _strike(p._strike),
  _type(p._type),
  _cost(p._cost)
{

}

GenericOption::~GenericOption() {

}

GenericOption &GenericOption::operator=(const GenericOption &p) 
{
    if (this != &p)
    {
        _type = p._type;
        _strike = p._strike;
        _cost = p._cost;
    }
    return *this;
}

PriceType GenericOption::valueAtExpiration(PriceType underlyingAtExpiration) 
{
    PriceType value{};

    if (_type == OptionType::OptionType_Call)
    {
        if (_strike < underlyingAtExpiration) 
        {
            value = underlyingAtExpiration - _strike;
        }
    }
    else 
    {
        if (_strike > underlyingAtExpiration)
        {
            value = _strike - underlyingAtExpiration;
        }
    }

    return value;
}

PriceType GenericOption::profitAtExpiration(PriceType underlyingAtExpiration)
{
    PriceType value{};
    PriceType finalValue = valueAtExpiration(underlyingAtExpiration);

    if (finalValue > _cost)
    {
        value = finalValue - _cost;
    }
    return value;
}