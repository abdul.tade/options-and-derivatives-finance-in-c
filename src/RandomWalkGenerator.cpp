# include "../include/RandomWalkGenerator.hpp"
# include <random>

std::vector<double> RandomWalkGenerator::generateWalk()
{
    std::vector<double> walk;
    double prev = _initialPrice;

    for (int i = 0; i < _numSteps; i++)
    {
        double val = computeRandomStep(prev);
        walk.push_back(val);
        prev = val;
    }

    return walk;
}

double RandomWalkGenerator::computeRandomStep(double currentPrice) 
{
    int r = rand() % _numDirections;
    double value = currentPrice;

    if (r == 0)
    {
        value += (_stepSize * value);
    }
    else if (r == 1)
    {
        value -= (_stepSize * value);
    }

    return value;
}