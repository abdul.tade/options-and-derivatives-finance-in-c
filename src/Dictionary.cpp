#include <iostream>
#include "../include/Dictionary.hpp"

void Dictionary::addElement(const std::string &s)
{
    if (s.size() != m_wordSize)
    {
        throw std::invalid_argument{"invalid string size"};
    }
    m_values.push_back(s);
    m_valuePositions[s] = static_cast<int>(m_values.size()-1);
    std::cout << " added \n";
}

std::string Dictionary::elemAtPos(int i)
{
    return m_values[i];
}

bool Dictionary::contains(const std::string &s)
{
    return m_valuePositions.find(s) != m_valuePositions.end();
}

int Dictionary::elemPosition(const std::string &s)
{
    return m_valuePositions[s];
}

void Dictionary::buildAdjacencyMatrix()
{
    m_adjacencyList.clear();
    int n = static_cast<int>(m_values.size());

    for (int i = 0; i < n; i++) 
    {
        m_adjacencyList.push_back(std::vector<bool>(n));
        for (int j = 0; j < n; j++)
        {
            if (diffByOne(m_values[i],m_values[j]))
            {
                m_adjacencyList[i][j] = true;
            }
        }
    }
}

bool diffByOne(const std::string &a, const std::string &b)
{
    if (a.size() != b.size()) {
        return false;
    }
    
    int ndiff = 0;
    for (unsigned i = 0; i < a.length(); ++i)
    {
        if (a[i] != b[i]) {
            ndiff++;
        }
    }

    return ndiff == 1;
}