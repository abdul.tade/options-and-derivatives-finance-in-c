# include "../include/DateCompact.hpp"
# include <cstring>
# include <iostream>

DateCompact::DateCompact(int year, int month, int day)
{
    setYear(year);
    setMonth(month);
    setDay(day);
}

DateCompact::DateCompact(const DateCompact& p)
{
    std::copy(p._date, p._date+8, _date);
}

DateCompact::~DateCompact() = default;

DateCompact &DateCompact::operator=(const DateCompact &p)
{
    if (&p != this) 
    {
        std::copy(p._date, p._date+8, _date);
    }
    return *this;
}

bool DateCompact::operator==(const DateCompact &d) const
{
    return std::equal(_date, _date+8, d._date, d._date+8);
}

bool DateCompact::operator<(const DateCompact &d) const 
{
    return std::strncmp(_date, d._date, 8) < 0;
}

int DateCompact::year()
{
    return  1000 * (_date[0] - '0') + 100 * (_date[1] - '0') + 10 * (_date[2] - '0') + (_date[3] - '0');
}

int DateCompact::month()
{
    return 10 * (_date[4] - '0') + (_date[5] - '0');
}

int DateCompact::day()
{
    return 10 *(_date[6] - '0') + (_date[7] - '0');
}

void DateCompact::print()
{
    char s[9] = {0};
    std::copy(_date, _date+8, s);
    std::cout << s << "\n";
}

void DateCompact::setYear(int year)
{
    _date[3] = '0' + (year % 10);
    year /= 10;
    _date[2] = '0' + (year % 10);
    year /= 10;
    _date[1] = '0' + (year % 10);
    year /= 10;
    _date[0] = '0' + (year % 10);
} 

void DateCompact::setMonth(int month)
{
    _date[5] = '0' + (month % 10); month /= 10;
    _date[4] = '0' + (month % 10); month /= 10;
}

void DateCompact::setDay(int day)
{
    _date[7] = '0' + (day % 10); day /= 10;
    _date[6] = '0' + (day % 10); day /= 10;
} 