# ifndef __RANDOM_WALK_GENERATOR__
# define __RANDOM_WALK_GENERATOR__

# include <vector>

class RandomWalkGenerator 
{

public:

    RandomWalkGenerator(int size, double start, double step)
        : _stepSize(step),
          _initialPrice(start),
          _numSteps(size)
        {}
    
    RandomWalkGenerator(const RandomWalkGenerator &p) 
        : _stepSize(p._stepSize),
          _initialPrice(p._initialPrice),
          _numSteps(p._numSteps)
        {}

    RandomWalkGenerator &operator=(const RandomWalkGenerator &p)
    {
        if (this != &p)
        {
            _numSteps = p._numSteps;
            _stepSize = p._stepSize;
            _initialPrice = p._initialPrice;
        }
        return *this;
    }
    
    virtual ~RandomWalkGenerator() = default;

    std::vector<double> generateWalk();

private:

    double _stepSize;
    double _initialPrice;
    int _numSteps;
    int _numDirections{3};

    double computeRandomStep(double currentPrice);
};

# endif // __RANDOM_WALK_GENERATOR__