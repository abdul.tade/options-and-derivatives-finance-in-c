# ifndef __GENERIC_OPTION__
# define __GENERIC_OPTION__

# include "OptionType.hpp"

using PriceType = double;

class GenericOption {
    public:
        GenericOption(PriceType strike, OptionType type, PriceType cost);
        GenericOption(const GenericOption &p);
        ~GenericOption();
        GenericOption& operator=(const GenericOption &p);

        PriceType valueAtExpiration(PriceType underlyingAtExpiration);
        PriceType profitAtExpiration(PriceType underlyingAtExpiration);

    private:
        PriceType _strike;
        OptionType _type;
        PriceType _cost;
};

# endif // __GENERIC_OPTIONS__