#include <string>
#include <vector>
#include <map>

class Dictionary
{
public:
    Dictionary(int wordSize);
    ~Dictionary(){}
    Dictionary &operator=(const Dictionary &p);
    void addElement(const std::string &s);
    void buildAdjacencyMatrix();
    bool contains(const std::string &s);
    const std::vector<std::vector<bool>> &adjList();
    int elemPosition(const std::string &s);
    int size() { return static_cast<int>(m_values.size());}
    std::string elemAtPos(int i);

private:
    std::vector<std::string> m_values;
    std::map<std::string, int> m_valuePositions;
    std::vector<std::vector<bool>> m_adjacencyList;
    int m_wordSize;
};