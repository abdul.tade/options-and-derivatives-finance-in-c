# ifndef __DATE_COMPACT__
# define __DATE_COMPACT__

class DateCompact 
{
public:
    DateCompact(int year, int month, int day);
    DateCompact(const DateCompact &p);
    virtual ~DateCompact();
    DateCompact &operator=(const DateCompact &p);

    void setYear(int y);
    void setMonth(int m);
    void setDay(int d);

    int year();
    int month();
    int day();

    void print();

    bool operator==(const DateCompact &d) const;
    bool operator<(const DateCompact &d) const;

    private:
        char _date[8];
};

# endif //__DATE_COMPACT__