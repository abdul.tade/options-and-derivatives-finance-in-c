CXX=clang++
CXXFLAGS= -Wall -std=c++2b

.PHONY: all build run

all:

	make build
	-@echo "++++++++++++++ OUTPUT +++++++++++++++++"
	make run
	-@echo "++++++++++++++ OUTPUT +++++++++++++++++"

main.o: src/main.cpp

	$(CXX) -c -o build/main.o src/main.cpp $(CXXFLAGS)

GenericOption.o: src/GenericOption.cpp
	
	$(CXX) -c -o build/GenericOption.o src/GenericOption.cpp $(CXXFLAGS)

RandomWalkGenerator.o: src/RandomWalkGenerator.cpp

	$(CXX) -c -o build/RandomWalkGenerator.o src/RandomWalkGenerator.cpp $(CXXFLAGS)

Date.o: src/Date.cpp

	$(CXX) -c -o build/Date.o src/Date.cpp $(CXXFLAGS)

DateCompact.o: src/DateCompact.cpp

	$(CXX) -c -o build/DateCompact.o src/DateCompact.cpp $(CXXFLAGS)

main: build/main.o build/GenericOption.o build/RandomWalkGenerator.o build/Date.o

	$(CXX) -o build/main build/main.o build/GenericOption.o build/RandomWalkGenerator.o build/Date.o build/DateCompact.o

build:
	-@[ -d build/ ] || mkdir build/
	make main.o 
	make GenericOption.o
	make RandomWalkGenerator.o
	make Date.o
	make DateCompact.o
	make main

run: build/main

	./build/main

clean: build/

	rm -r build/

